//
//  AppDelegate.h
//  Davanzati Hotel
//
//  Created by Developer on 11/16/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "UserEntity.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UserEntity *Me;
    MBProgressHUD *HUD;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) UserEntity * Me;

@property (nonatomic, strong) NSString *_deviceToken;

- (void) showLoadingViewWithTitle:(NSString *) title sender:(id) sender;

- (void) hideLoadingView;

@end

