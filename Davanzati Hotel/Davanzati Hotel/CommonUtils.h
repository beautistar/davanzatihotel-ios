//
//  CommonUtils.h
//  Davanzati Hotel
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDefault.h"
#import "PrefConst.h"
#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>

@interface CommonUtils : NSObject

+ (void) saveuserInfo;
+ (void) loadUserInfo;

+ (void) setUserIdx:(int) idx;
+ (int) getUserIdx;

+ (void) setUserLoggedIn :(BOOL) isLoggedIn;
+ (BOOL) getUserLogin;

+ (void) saveDeviceToken : (NSString *) deviceToken;
+ (NSString *) getDeviceToken;

+ (void) vibrate;

@end
