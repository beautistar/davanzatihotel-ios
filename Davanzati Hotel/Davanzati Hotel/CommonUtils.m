//
//  CommonUtils.m
//  Davanzati Hotel
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CommonUtils.h"

@implementation CommonUtils

+ (void) saveuserInfo {
    
    [CommonUtils setUserIdx:APPDELEGATE.Me.idx];
    
}

+ (void) loadUserInfo {
    
    if (APPDELEGATE.Me) {
        
        APPDELEGATE.Me.idx = [CommonUtils getUserIdx];
    }    
}

+ (void) setUserIdx:(int)idx {
    
    [UserDefault setIntValue:PREFKEY_ID value:idx];
    
}

+ (int) getUserIdx {
    
    return [UserDefault getIntValue:PREFKEY_ID];
}

+ (void) setUserLoggedIn:(BOOL)isLoggedIn {
    
    [UserDefault setBoolValue:PREFKEY_LOGGED_IN value:isLoggedIn];
}

+ (BOOL) getUserLogin {
    
    return [UserDefault getBoolValue:PREFKEY_LOGGED_IN];
}

+ (void) saveDeviceToken:(NSString *)deviceToken {
    
    [UserDefault setStringValue:PREFKEY_DEVICE_TOKEN value:deviceToken];
}

+ (NSString *) getDeviceToken {
    
    
    return [UserDefault getStringValue:PREFKEY_DEVICE_TOKEN];
}

+ (void) vibrate {
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

@end
