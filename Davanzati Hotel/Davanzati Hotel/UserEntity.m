//
//  UserEntity.m
//  Davanzati Hotel
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "UserEntity.h"

@implementation UserEntity

@synthesize idx, deviceToken;

- (instancetype) init {
    
    if (self = [super init]) {
        
        idx = 0;
        deviceToken = @"";       
        
    }
    
    return self;
}

- (BOOL) isValid {
    
    if (idx > 0) {
        
        return YES;
    }
    
    return NO;
}

@end
