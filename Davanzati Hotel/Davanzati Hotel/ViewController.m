//
//  ViewController.m
//  Davanzati Hotel
//
//  Created by Developer on 11/16/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ViewController.h"
#import "WebViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.    

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerAction:(id)sender {
    
    [self gotoWebView];
}

- (void) gotoWebView {
    
     ([UIApplication sharedApplication].keyWindow).rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    
}

@end
