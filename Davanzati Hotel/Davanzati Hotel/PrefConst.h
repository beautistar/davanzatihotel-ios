//
//  PrefConst.h
//  Davanzati Hotel
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#ifndef PrefConst_h
#define PrefConst_h

//#define SERVER_URL              @"http://35.163.221.156/index.php/api/"
#define SERVER_URL                  @"http://79.6.223.56/davanzatihotel/index.php/api/"
#define REQ_REGISTERTOKEN           @"registerToken"

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define PREFKEY_ID              @"user_id"

#define PREFKEY_LOGGED_IN       @"user_logged_in"

#define PREFKEY_DEVICE_TOKEN    @"device_token"

#define PREFKEY_BADGECOUNT      @"BADGECOUNT"

#define RES_CODE                @"result_code"
#define RES_ID                  @"id"
#define CODE_SUCCESS            0


#endif /* PrefConst_h */
