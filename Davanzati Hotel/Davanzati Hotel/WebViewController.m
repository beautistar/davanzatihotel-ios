//
//  WebViewController.m
//  Davanzati Hotel
//  Created by Developer on 11/16/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController () {
    
    __weak IBOutlet UIWebView *webView;
}

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
}

- (void) initLoad {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    NSString *fullURL = @"http://notifications.hoteldavanzati.it";
    
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
