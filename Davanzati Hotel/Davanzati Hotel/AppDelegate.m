//
//  AppDelegate.m
//  Davanzati Hotel
//
//  Created by Developer on 11/16/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "AppDelegate.h"
#import "CommonUtils.h"
#import <AFNetworking/AFNetworking.h>


@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize Me, _deviceToken;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    Me = [[UserEntity alloc] init];
    _deviceToken = @"";
    
    [CommonUtils getUserIdx];
    
//    if(launchOptions != nil) {
//        
//        NSDictionary * userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//        
//        if (userInfo != nil) {
//            
//            [self application:application didReceiveRemoteNotification:userInfo];
//        }
//    }
//    
//    [USERDEFAULTS setInteger:[UIApplication sharedApplication].applicationIconBadgeNumber forKey:PREFKEY_BADGECOUNT];
//    
//    [self.window makeKeyAndVisible];
    NSLog(@"Registrating for push notifications...");
    
    
    ////
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
    
    [application registerUserNotificationSettings:settings];
    
//    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString *token = [deviceToken.description stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"device_token : %@", token);
    
    //[UserDefault setStringValue:PREFKEY_DEVICE_TOKEN value:token];
    
    //register token
    
    [self registerGCMDeviceToken:token];
    
}

- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    NSString *str = [NSString stringWithFormat:@"Error : %@", error];
    NSLog(@"Error : %@", str);
}


- (void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
     //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"Notification received: %@", userInfo);
    
    [USERDEFAULTS setInteger:[UIApplication sharedApplication].applicationIconBadgeNumber forKey:@"BADGECOUNT"];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = [[userInfo valueForKey:@"aps"] valueForKey:@"sound"];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
//                                                        object:nil
//                                                      userInfo:userInfo];
//    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo valueForKey:@"aps"] valueForKey:@"badge"] intValue];
//    
//    [badgeDelegate didReceived];
//    
//    handler(UIBackgroundFetchResultNoData);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
 
    //NSNotification* _notification = [NSNotification notificationWithMessage:notification.alertBody];
    
    [CommonUtils vibrate];
    //[[NSNotificationCenter defaultCenter] presentNotification:_notification forApplicationIdentifier:@"123"];
    
}

- (void) showLoadingViewWithTitle:(NSString *)title sender:(id)sender {
    
    HUD = [[MBProgressHUD alloc] initWithView:self.window];
    
    [self.window addSubview:HUD];
    HUD.minSize = CGSizeMake(100.f, 100.f);
    // Set the hud to display with a color
    HUD.color = [UIColor colorWithRed:234/255.0 green:76/255.0 blue:136/255.0 alpha:0.00];
    //    HUD.color = [UIColor colorWithPatternImage:nil];
    
    HUD.delegate = sender;
    HUD.labelText = title;
    
    [HUD show:YES];
}

- (void) hideLoadingView {
    
    [HUD hide:YES];
}

- (void) hideLoadingView : (NSTimeInterval) delay {
    
    [HUD hide:YES afterDelay:delay];
}


- (void) registerGCMDeviceToken:(NSString *) deviceTokenGCM {
    
    if(![deviceTokenGCM isEqualToString:@""])
        [CommonUtils saveDeviceToken:deviceTokenGCM];
    
    NSLog(@"token %@", deviceTokenGCM);
    
    // get my phone UDID
    NSString *udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *udid1 = [udid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *r_udid = [NSString stringWithFormat:@"%@%@", @"FFFFFFFF", udid1];
//    NSString *udid = [[UIDevice currentDevice] identifierForVendor];
    
    NSUUID *uuid3 = [[NSUUID alloc]initWithUUIDString:udid];
    NSLog(@"%@",uuid3);
    NSLog(@"%@",[uuid3 UUIDString]);
    
    NSLog(@"udid : %@", r_udid);
    
    NSString *adId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    if([Me isValid] || deviceTokenGCM == nil) return;
    if ([CommonUtils getUserLogin]) return;
    
    [self showLoadingViewWithTitle:nil sender:self];
    //make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%i/%@", SERVER_URL, REQ_REGISTERTOKEN, 1, deviceTokenGCM];
    NSString * escapeURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"register token - %@", escapeURL);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapeURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        [self hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSLog(@"register token to server");
            
            Me.idx = [[responseObject valueForKey:RES_ID] intValue];
            [CommonUtils saveuserInfo];
            [CommonUtils setUserLoggedIn:YES];
            
        } else  {
            
            NSLog(@"fail to register token");
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
}

@end
