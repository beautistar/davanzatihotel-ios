//
//  UserEntity.h
//  Davanzati Hotel
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserEntity : NSObject

@property (nonatomic) int idx;
@property (nonatomic, strong) NSString *deviceToken;
@property (NS_NONATOMIC_IOSONLY, getter=isValid, readonly) BOOL valid;

@end
